import unittest

class myFirstTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('Class setUp!')

    def setUp(self):
        print('SetUp call!')

    @classmethod
    def tearDownClass(cls):
        print('Class tearDown!')

    def tearDown(self):
        print('TearDown call!')

    def test_1(self):
        print('Test 1')
        self.assertEqual(3, 2)

    def test_2(self):
        print('Test 2')
        self.assertEqual(2, 2)

    def test_3(self):
        print('Test 3')
        print('wtf')
