import unittest
from test import myFirstTest

def suite():
    suite=unittest.TestSuite()
    suite.addTest(myFirstTest('test_1'))
    return suite

if __name__=='__main__':
    runner = unittest.TextTestRunner()
    runner.run(suite())