import unittest
from account import Account
from card import Card

class MyTestCase(unittest.TestCase):

    def setUp(self) -> None:
        self.account = Account('1234567890', 'Łukasz', 'Węglarz', 15000)
        self.card = Card(self.account, 1234)

    def test_account_should_return_account_number(self):
        self.assertEqual(self.account(), "1234567890", 'Account number not match.')

    def test_owner_should_return_names(self):
        self.assertEqual(self.account.owner(), 'Łukasz Węglarz', 'Names not match.')

    def test_balance_should_return_current_balance(self):
        self.assertEqual(self.account.balance(), 15000, 'Account number not match.')

    def test_number_should_return_account_number(self):
        self.assertEqual(self.account.number(), '1234567890', 'Account number not match.')

    def test_account_transfer_should_change_account_balance(self):
        self.account.transfer(15000)
        self.assertEqual(self.account.balance(), 30000, 'Account balance not match.')

    @unittest.skip('')
    def test_card_should_return_name_of_the_owner(self):
        self.assertEqual(self.card(), 'Łukasz Węglarz', 'Names not match.')

    def test_check_pin_should_check_if_pin_is_correct(self):
        self.assertTrue(self.card.check_pin(1234))

    def test_get_account_should_return_account_it_refers_to(self):
        self.assertIs(self.card.get_account(), self.account)

if __name__ == '__main__':
    unittest.main()