from account import *
from card import *
import unittest

class TddZad1(unittest.TestCase):
    # 1. Sprawdzić czy saldo jest mniejsze niż wypłata
    # 2. Sprawdzić czy można wypłacić, jeśli pin jest niepoprawny
    # 3. Chcę przelać pieniadze na inne konto, tym samym obniżyć swoje saldo i zwiększyć obce
    # 4. Chcę sprawdzić czy moge przelać pieniądze (czy nie mam za mało)
    # 5. Chcę uniemożliwić zlecenia przelewu z obcego konta na moje
    # 6. Chcę aby wrócił komunizm

    def test_balance_should_be_greater_than_0(self):
        account = Account('1234567890', 'Łukasz', 'Węglarz', 1000)
        card=Card(account, '1234')
        self.assertEqual(card.withdraw(1001, '1234'), 0)

    def test_withdraw_money(self):
        account = Account('1234567890', 'Łukasz', 'Węglarz', 1000)
        card = Card(account, '1234')

        self.assertEqual(card.withdraw(1000, '1234'), 0)
        self.assertEqual(card.account.balance(), 0)

    def test_withdraw_pin_incorrect(self):
        account = Account('1234567890', 'Łukasz', 'Węglarz', 1000)
        card = Card(account, '1234')

        self.assertEqual(card.withdraw(50, '0000'), 0)

    def test_sendMoney_should_give_money_to_another_account(self):
        account1 = Account('1234567890', 'Łukasz', 'Węglarz', 500)
        account2 = Account('0987654321', 'Wojciech', 'Węglarz', 1000)

        account2.sendMoney(250, account1)

        self.assertEqual(account1.balance(), 750)
        self.assertEqual(account2.balance(), 750)

    def test_sendMoney_should_not_allow_to_give_more_money_than_account_has(self):
        account1 = Account('1234567890', 'Łukasz', 'Węglarz', 500)
        account2 = Account('0987654321', 'Wojciech', 'Węglarz', 1000)

        account2.sendMoney(1001, account1)

        self.assertEqual(account1.balance(), 500)
        self.assertEqual(account2.balance(), 1000)

    def test_do_not_send_money_from_recipeint_to_sender(self):
        account1 = Account('1234567890', 'Łukasz', 'Węglarz', 500)
        account2 = Account('0987654321', 'Wojciech', 'Węglarz', 1000)

        with self.assertRaises(UnsuportedTransactionError):
            account2.sendMoney(-1000, account1)

    def test_card_should_decline_acces_after_3_incoreect_pin_attempts(self):
        account = Account('1234567890', 'Łukasz', 'Węglarz', 1000)
        card = Card(account, '1234')

        with self.assertRaises(LoginError):
            [card.check_pin('0000') for i in range(4)]